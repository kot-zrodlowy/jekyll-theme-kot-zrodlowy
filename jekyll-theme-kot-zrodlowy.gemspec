# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = "jekyll-theme-kot-zrodlowy"
  spec.version       = "0.1.5"
  spec.authors       = ["Kornelia Kobiela"]
  spec.email         = ["kornelia.kobiela@kot-zrodlowy.pl"]

  spec.summary       = "Just Jekyll theme for https://kot-zrodlowy.pl blog."
  spec.homepage      = "https://gitlab.com/kot-zrodlowy/kot-zrodlowy-jekyll-theme"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r!^(assets|_layouts|_includes|_sass|LICENSE|README)!i) }

  spec.add_runtime_dependency "jekyll", "~> 3.8"

  spec.add_development_dependency "bundler", ">= 2.0.1"
  spec.add_development_dependency "rake", "~> 12.0"
end
